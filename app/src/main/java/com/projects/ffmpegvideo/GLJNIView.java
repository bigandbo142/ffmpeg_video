package com.projects.ffmpegvideo;

import android.content.Context;
import android.opengl.GLSurfaceView;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/**
 * Created by skywanderer on 04/04/2016.
 *
 */

public class GLJNIView extends GLSurfaceView {
    public GLJNIView(Context pContext){
        super(pContext);
        // Pick an EGLConfig with RGB8 color, 16-bit depth, no stencil, supporting OpenGL 2.0 and later
        setEGLConfigChooser(8, 8, 8, 0, 16, 0);
        setEGLContextClientVersion(2);
        setRenderer(new MyRenderer());
    }

    private static class MyRenderer implements Renderer{
        @Override
        public void onSurfaceCreated(GL10 gl, EGLConfig config) {
            // @TODO: call function to initialize screen for rendering frame
        }

        @Override
        public void onSurfaceChanged(GL10 gl, int width, int height) {

        }

        @Override
        public void onDrawFrame(GL10 gl) {
            // @TODO: Call jni function to draw frame on screen
        }
    }
}
