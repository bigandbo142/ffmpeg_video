package com.projects.ffmpegvideo;

/**
 * Created by skywanderer on 29/03/2016.
 */
public class VideoNativeLib {
    public static native int loadFile(String pFilename);
    public static native int doSetup(int pWidth, int pHeight, String pOutput);
}
