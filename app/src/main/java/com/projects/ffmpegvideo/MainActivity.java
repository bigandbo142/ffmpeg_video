package com.projects.ffmpegvideo;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    public final static String PATH_SAVE = Environment.getExternalStorageDirectory() + "/Android/data/" + BuildConfig.APPLICATION_ID + "/tmp";
    private Button btn_choose;

    static {
        System.loadLibrary("ffmpeg");
        System.loadLibrary("nativevideo");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btn_choose = (Button) findViewById(R.id.btn_choose);
        btn_choose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
                intent.setType("video/*");
                startActivityForResult(Intent.createChooser(intent, "Select File"), 123);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 123){
            Uri selectedImage = data.getData();
            String path = getRealPathFromURI(selectedImage);
            if(VideoNativeLib.loadFile(path) >= 0){
                File tmpDir = new File(PATH_SAVE);
                if(!tmpDir.exists()){
                    tmpDir.mkdirs();
                }
                try {
                    File tmpFile = new File(PATH_SAVE, "tmp.yuv");
                    if (tmpFile.exists()) {
                        if (tmpFile.delete())
                            tmpFile.createNewFile();
                    }
                    if (VideoNativeLib.doSetup(480, 480, tmpFile.getPath().toString()) >= 0) {
                        Toast.makeText(this, "Setup successful", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(this, "Setup failed", Toast.LENGTH_SHORT).show();
                    }
                }catch (IOException ex){
                    ex.printStackTrace();
                }
            }else{
                Toast.makeText(this, "Cannot open video file", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Video.VideoColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }
}
