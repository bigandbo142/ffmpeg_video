#ifndef TEXTURE_HPP
#define TEXTURE_HPP

#include <GLES2/gl2.h>

// Load an image file to the texture

class Texture {
public:
    Texture();
    virtual ~Texture();
    GLuint initializeTexture(const unsigned char * pImageData, int pWidth, int pHeight);
    void updateTexture(const unsigned char * pImageData, int pWidth, int pHeight, GLenum pFormat);
};

#endif