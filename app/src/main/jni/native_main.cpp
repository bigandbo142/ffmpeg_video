//
// Created by skywanderer on 29/03/2016.
//

#include <stdio.h>
#include <time.h>
#include <wchar.h>

//header for the OpenGL ES2 library
#include <GLES2/gl2.h>
// header for ffmpeg
#ifdef __cplusplus
#define __STDINT_MACROS
#define __STDC_CONSTANT_MACROS
#define UINT64_C
extern "C" {
#endif
#include "libavcodec/avcodec.h"
#include "libavformat/avformat.h"
#include "libswscale/swscale.h"
#include "libavutil/log.h"
#include "libavutil/pixfmt.h"
#include "libavutil/imgutils.h"
#ifdef __cplusplus
}
#endif


#ifdef ANDROID
#include <jni.h>
#include <android/log.h>
#define LOGE(format, ...)  __android_log_print(ANDROID_LOG_ERROR, "(>_<)", format, ##__VA_ARGS__)
#define LOGI(format, ...)  __android_log_print(ANDROID_LOG_INFO,  "(^_^)", format, ##__VA_ARGS__)
#else
#define LOGE(format, ...)  printf("(>_<) " format "\n", ##__VA_ARGS__)
#define LOGI(format, ...)  printf("(^_^) " format "\n", ##__VA_ARGS__)
#endif

// Declare variables
AVFormatContext     *mFormatContext;
AVCodecContext      *mCodecContext;
AVCodec             *mCodec;
AVFrame             *mFrame, *mFrameYUV;
AVPacket            *mPacket;
FILE                *mFileOutYUV;
jobject             *mBitmap;
uint8_t             *mOutBuffer;
void                *mBitmapBuffer;
int                 mVideoIndex;
int                 mFrameCnt;
struct SwsContext   *mImg_convert_context;
char                mInput_str[500] = {0};
char                mOutput_str[500] = {0};
char                mInfo[1000] = {0};
int                 mYSize;
int                 mDestWidth;
int                 mDestHeight;

// Shader program and all handlers
GLuint mProgram;
GLuint mPositionHandle;
GLuint mColorHandle;

// Vertex shader source code
static const char mVShaderCode[] =
        "#version 100 es\n"
        "in vec4 vPosition;\n"
        "in vec4 vColor;\n"
        "out vec4 color;\n"
        "void main(){\n"
        "   gl_Position = vPosition;\n"
        "   color = vColor;\n"
        "}\n";

// Fragment shader source code
static const char mFShaderCode[] =
        "#version 100 es\n"
        "precision mediump float;\n"
        "in vec4 color;\n"
        "out vec4 color_out;\n"
        "void main(){\n"
        "   color_out = color;\n"
        "}\n";


/**
 * Initialization and call upon changes to graphics framebuffer
 *
 */

int setupGraphics(int pWidth, int pHeight){
//    printGLString("VERSION", GL_VERSION);
//    printGLString("VENDOR", GL_VENDOR);
//    printGLString("RENDERER", GL_RENDERER);
//    printGLString("EXTENSIONS", GL_EXTENSIONS);
//
//    LOGI("Set up graphics (%d, %d)", pWidth, pHeight);
//    mProgram = createShaderProgram(mVShaderCode, mFShaderCode);
//    if(!mProgram){
//        LOGE("Could not create program");
//        return -1;
//    }
//    mPositionHandle = glGetAttribLocation(mProgram, "vPosition");
//    checkGlError("glGetAttribLocation");
//    LOGI("glGetAttribLocation(\"vPosition\") = %d\n", mPositionHandle);
//    mColorHandle = glGetAttribLocation(mProgram, "vColor");
//    checkGlError("glGetAttribLocation");
//    LOGI("glGetAttribLocation(\"vColor\") = %d\n", mColorHandle);
//
//    glViewport(0, 0, pWidth, pHeight);
//    checkGlError("glViewport");
//    return 0;
}

//vertices
GLfloat mTriangle[9] = {
        -1.0f, -1.0f, 0.0f,
        1.0f, -1.0f, 0.0f,
        0.0f, 1.0f, 0.0f
};
GLfloat mColor[9] = {
        1.0f, 0.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 0.0f, 1.0f
};

/**
 * Call per render, perform graphics updates
 */

void renderFrame(){
//    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
//    checkGlError("glClearColor");
//
//    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
//    checkGlError("glClear");
//
//    glUseProgram(mProgram);
//    checkGlError("glUserProgram");
//
//    glVertexAttribPointer(mPositionHandle, 3, GL_FLOAT, GL_FALSE, 0, mTriangle);
//    checkGlError("glVertextAttribPointer");
//
//    glVertexAttribPointer(mColorHandle, 3, GL_FLOAT, GL_FALSE, 0, mColor);
//    checkGlError("glVertexAttribPointer");
//
//    glEnableVertexAttribArray(mPositionHandle);
//    checkGlError("glEnableVertexAttribArray");
//
//    glEnableVertexAttribArray(mColorHandle);
//    checkGlError("glEnableVertexAttribArray");
//
//    glDrawArrays(GL_TRIANGLES, 0, 9);
//    checkGlError("glDrawArrays");
}

//Output FFmpeg's av_log()
void custom_log(void *ptr, int level, const char* fmt, va_list vl){
    FILE *fp=fopen("/storage/emulated/0/av_log.txt","a+");
    if(fp){
        vfprintf(fp,fmt,vl);
        fflush(fp);
        fclose(fp);
    }
}

jobject createBitmap(JNIEnv * pEnv, int pWidth, int pHeight){
    int i;

    // get Bitmap class and createBitmap method ID
    jclass javaBitmapClass = (jclass) pEnv->FindClass("android/graphics/Bitmap");
    jmethodID methodID = pEnv->GetStaticMethodID(javaBitmapClass, "createBitmap", "(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;");

    // create Bitmap.config
    const wchar_t   *configName = L"ARGB_8888";
    int len = wcslen(configName);
    jstring     jConfigName;
    if(sizeof(wchar_t) != sizeof(jchar)){
        jchar   *str = (jchar*)malloc((len + 1) * sizeof(jchar));
        for(i = 0; i < len; i++){
            str[i] = (jchar)configName[i];
        }
        str[len] = 0;
        jConfigName = pEnv->NewString((const jchar*)str, len);
    }else{
        jConfigName = pEnv->NewString((const jchar*)configName, len);
    }
    jclass bitmapConfigClass = pEnv->FindClass("android/graphics/Bitmap$Config");
    jobject javaBitmapConfig = pEnv->CallStaticObjectMethod(
            bitmapConfigClass,
            pEnv->GetStaticMethodID(
                    bitmapConfigClass,
                    "valueOf",
                    "(Ljava/lang/String;)Landroid/graphics/Bitmap$Config;"
            ),
            jConfigName
    );

    // create bitmap
    return pEnv->CallStaticObjectMethod(javaBitmapClass, methodID, pWidth, pHeight, javaBitmapConfig);
}

/**
 * Flush decoder
 * Flush all frames remained in codec
 */

jint flushDecoder(){
    int ret;
    int got_frame;
    if(mFileOutYUV != NULL) {
        while (1) {
            ret = avcodec_decode_video2(mCodecContext, mFrame, &got_frame, mPacket);
            if (ret < 0) {
                break;
            }
            if (!got_frame) {
                break;
            }
            sws_scale(
                    mImg_convert_context,
                    (const uint8_t *const *) mFrame->data,
                    mFrame->linesize,
                    0,
                    mCodecContext->height,
                    mFrameYUV->data,
                    mFrameYUV->linesize
            );
//            int y_size = mCodecContext->width * mCodecContext->height;
            int y_size = mDestWidth * mDestHeight;
            fwrite(mFrameYUV->data[0], 1, y_size, mFileOutYUV);
            fwrite(mFrameYUV->data[1], 1, y_size / 4, mFileOutYUV);
            fwrite(mFrameYUV->data[2], 1, y_size / 4, mFileOutYUV);
        }
    }
}

jint saveFrameToFile(char *pFile){
    int got_frame;
    mFrameCnt = 0;

    // open output file for writing data
    mFileOutYUV = fopen(mOutput_str, "wb+");
    if(mFileOutYUV == NULL){
        LOGE("Cannot open outfile for writing data");
        return -1;
    }


    while (av_read_frame(mFormatContext, mPacket) >= 0){
        if(mPacket->stream_index == mVideoIndex){
            if(avcodec_decode_video2(mCodecContext, mFrame, &got_frame, mPacket) < 0){
                LOGE("Decode error");
                return -1;
            }

            if(got_frame){
                sws_scale(
                        mImg_convert_context,
                        (const uint8_t* const*) mFrame->data,
                        mFrame->linesize,
                        0,
                        mCodecContext->height,
                        mFrameYUV->data,
                        mFrameYUV->linesize
                );

//                mYSize = mCodecContext->width * mCodecContext->height;
                mYSize = mDestWidth * mDestHeight;

                // write data
                fwrite(mFrameYUV->data[0], 1, mYSize, mFileOutYUV); // Y
                fwrite(mFrameYUV->data[1], 1, mYSize / 4, mFileOutYUV); // U
                fwrite(mFrameYUV->data[2], 1, mYSize / 4, mFileOutYUV); // V

                char frametype_str[10] = {0};
                switch (mFrame->pict_type){
                    case AV_PICTURE_TYPE_I:
                        sprintf(frametype_str, "I");
                        break;
                    case AV_PICTURE_TYPE_P:
                        sprintf(frametype_str, "P");
                        break;
                    case AV_PICTURE_TYPE_B:
                        sprintf(frametype_str, "B");
                        break;
                    default:
                        sprintf(frametype_str, "other");
                        break;
                }
                LOGI("Frame index: %5d. Type: %s", mFrameCnt, frametype_str);
                mFrameCnt++;
            }
        }

        av_free_packet(mPacket);
    }
    LOGI("Frame count %d", mFrameCnt);

    // free all memory
    flushDecoder();
    sws_freeContext(mImg_convert_context);
    fclose(mFileOutYUV);
    av_frame_free(&mFrame);
    av_frame_free(&mFrameYUV);
    avcodec_close(mCodecContext);
    avformat_close_input(&mFormatContext);

    return 0;
}

// external java call
extern "C" {
    JNIEXPORT jint JNICALL Java_com_projects_ffmpegvideo_VideoNativeLib_loadFile(JNIEnv * pEnv, jobject pObj, jstring pFilename);
    JNIEXPORT jint JNICALL Java_com_projects_ffmpegvideo_VideoNativeLib_doSetup(JNIEnv * pEnv, jobject pObj, int pWidth, int pHeight, jstring pOutputFile);

    JNIEXPORT void JNICALL Java_com_projects_ffmpegvideo_VideoNativeLib_doInitGraphics(JNIEnv * pEnv, jobject pObj, jint pWidth, jint pHeight);
    JNIEXPORT void JNICALL Java_com_projects_ffmpegvideo_VideoNativeLib_doStep(JNIEnv * pEnv, jobject pObj);
}

JNIEXPORT jint JNICALL Java_com_projects_ffmpegvideo_VideoNativeLib_loadFile(JNIEnv * pEnv, jobject pObj, jstring pFilename){
    sprintf(mInput_str,"%s",(pEnv)->GetStringUTFChars(pFilename, JNI_FALSE));
    // Register all formats and codecs
    LOGI("vui velog %s", mInput_str);

    // set FFMPEG av_log callback
    av_log_set_callback(custom_log);

    av_register_all();
    avformat_network_init();

    // Allocate format context
    mFormatContext = avformat_alloc_context();

    if(avformat_open_input(&mFormatContext, mInput_str, NULL, NULL) != 0) {
        LOGE("Couldn't open input stream");
        return -1;
    }

    if(avformat_find_stream_info(mFormatContext, NULL) < 0) {
        LOGE("Couldn't find stream information");
        return -1;
    }

    mVideoIndex = -1;
    for(int i = 0; i < mFormatContext->nb_streams; i++){
        if(mFormatContext->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO){
            mVideoIndex = i;
            break;
        }
    }

    if(mVideoIndex == -1){
        LOGE("Couldn't find a video stream");
        return -1;
    }

    mCodecContext = mFormatContext->streams[mVideoIndex]->codec;
    mCodec = avcodec_find_decoder(mCodecContext->codec_id);

    if(mCodec == NULL){
        LOGE("Couldn't find codec");
        return -1;
    }

    if(avcodec_open2(mCodecContext, mCodec, NULL) < 0) {
        LOGE("Couldn't open codec");
        return -1;
    }

    mFrame = av_frame_alloc();
    mFrameYUV = av_frame_alloc();

    if(mFrameYUV == NULL){
        LOGE("Couldn't alloc memory for frame RGB");
        return -1;
    }
    return 0;
}

JNIEXPORT jint JNICALL Java_com_projects_ffmpegvideo_VideoNativeLib_doSetup(JNIEnv * pEnv, jobject pObj, int pWidth, int pHeight, jstring pOutputFile){
    if(mCodecContext == NULL){
        LOGE("Codec context was not initialized");
        return -1;
    }

    mDestWidth = pWidth;
    mDestHeight = pHeight;

    // create bitmap as the buffer for frameRGBA
//    mBitmap = createBitmap(pEnv, pWidth, pHeight);
//    if(AndroidBitmap_lockPixels(pEnv, mBitmap, &mBitmapBuffer) > 0){
//        return -1;
//    }

    mPacket=(AVPacket *)av_malloc(sizeof(AVPacket));

    // get the scaling context
    mImg_convert_context = sws_getContext (
            mCodecContext->width,
            mCodecContext->height,
            mCodecContext->pix_fmt,
            mDestWidth,
            mDestHeight,
            AV_PIX_FMT_RGBA,
            SWS_BILINEAR,
            NULL,
            NULL,
            NULL
    );

    mOutBuffer = (unsigned char*) av_malloc(
            av_image_get_buffer_size(
                    AV_PIX_FMT_YUV420P,
                    mDestWidth, //mCodecContext->width,
                    mDestHeight, //mCodecContext->height,
                    1
            )
    );

    av_image_fill_arrays(
            mFrameYUV->data,
            mFrameYUV->linesize,
            mOutBuffer,
            AV_PIX_FMT_YUV420P,
            mDestWidth, //mCodecContext->width,
            mDestHeight, //mCodecContext->height,
            1
    );
    sprintf(mOutput_str,"%s",(pEnv)->GetStringUTFChars(pOutputFile, JNI_FALSE));
    saveFrameToFile(mOutput_str);
}

JNIEXPORT void JNICALL Java_com_projects_ffmpegvideo_VideoNativeLib_doInitGraphics(JNIEnv * pEnv, jobject pObj, jint pWidth, jint pHeight){
    setupGraphics(pWidth, pHeight);
}

JNIEXPORT void JNICALL Java_com_projects_ffmpegvideo_VideoNativeLib_doStep(JNIEnv * pEnv, jobject pObj){
    renderFrame();
}