#include "Texture.hpp"

Texture::Texture() { }
Texture::~Texture() { }

GLuint Texture::initializeTexture(const unsigned char *pImageData, int pWidth, int pHeight) {
    GLuint textureId = 0;

    // for the first time we create the image
    // create one texture element
    glGenTextures(1, &textureId);

    // bind the one element
    glBindTexture(GL_TEXTURE_2D, textureId);

    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

    // Specifies the target texture and the parameters describe the format and type of the image data
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, pWidth, pHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, pImageData);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

    glGenerateMipmap(GL_TEXTURE_2D);
    return  textureId;
}

void Texture::updateTexture(const unsigned char *pImageData, int pWidth, int pHeight,
                            GLenum pFormat) {
    // Update the texture
    glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, pWidth, pHeight, pFormat, GL_UNSIGNED_BYTE, pImageData);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

    glGenerateMipmap(GL_TEXTURE_2D);
}

