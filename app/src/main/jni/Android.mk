# Android.mk for Android
# Created by Skywanderer

LOCAL_PATH := $(call my-dir)

# FFMPEG Library
include $(CLEAR_VARS)
LOCAL_MODULE := ffmpeg
LOCAL_SRC_FILES := libffmpeg.so
include $(PREBUILT_SHARED_LIBRARY)

# Main local library
include $(CLEAR_VARS)
LOCAL_MODULE := nativevideo
LOCAL_SRC_FILES := native_main.cpp Shader.cpp Texture.cpp
LOCAL_C_INCLUDES += $(LOCAL_PATH)/include
LOCAL_LDLIBS := -llog -ljnigraphics -lz -landroid -lGLESv2
LOCAL_SHARED_LIBRARIES += ffmpeg
include $(BUILD_SHARED_LIBRARY)